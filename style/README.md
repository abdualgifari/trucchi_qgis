## Somma aree selezione / sum selected areas

Sfruttando la nuova [caratteristica](https://twitter.com/lutraconsulting/status/1057981333413208064) della QGIS 3.4, seleziono un poligono e QGIS seleziona tutti gli altri adiacenti; poi faccio etichettare - in un sol punto - la somma delle arre dei poligoni selezionati.

Taking advantage of the new [feature](https://twitter.com/lutraconsulting/status/1057981333413208064) of QGIS 3.4, I select a polygon and QGIS selects all the others adjacent; then I have to label - in a single point - the sum of the ares of the selected polygons.

![img](./img_01.png)

![img](./sum_area_selected.gif)

[file qml](sum_area_group_features.qml)


## Conta punti selezione / count selected points

![img](./img_02.png)

![img](./count_point.gif)

[file qml](count_group_points.qml)
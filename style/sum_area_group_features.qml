<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.5.0-Master" styleCategories="Labeling" labelsEnabled="1">
  <labeling type="rule-based">
    <rules key="{54d57e3b-ce59-432a-bc0a-99517f09745e}">
      <rule filter="is_selected()" key="{4d16baf3-a545-4d95-a735-601e11374913}">
        <settings>
          <text-style textOpacity="1" fontStrikeout="0" fontLetterSpacing="0" fontSize="15" fontUnderline="0" fontCapitals="0" fontSizeUnit="Point" fontSizeMapUnitScale="3x:0,0,0,0,0,0" multilineHeight="1" fontWordSpacing="0" fontItalic="0" useSubstitutions="0" isExpression="1" fontFamily="MS Shell Dlg 2" fieldName=" format_number( sum($area, group_by:=is_selected())/1000000,0)||' km²'" textColor="255,255,255,255" blendMode="0" fontWeight="50" previewBkgrdColor="#ffffff" namedStyle="Normale">
            <text-buffer bufferSize="1" bufferJoinStyle="128" bufferSizeUnits="MM" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferBlendMode="0" bufferDraw="1" bufferColor="0,0,0,255" bufferNoFill="1"/>
            <background shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeType="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="MM" shapeOffsetY="0" shapeBlendMode="0" shapeSizeType="0" shapeRotation="0" shapeRadiiUnit="MM" shapeFillColor="255,255,255,255" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeSVGFile="" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="MM" shapeRadiiX="0" shapeSizeY="0" shapeSizeX="0" shapeRotationType="0" shapeBorderWidthUnit="MM" shapeDraw="0" shapeBorderWidth="0" shapeJoinStyle="64" shapeRadiiY="0" shapeOffsetX="0" shapeBorderColor="128,128,128,255" shapeOpacity="1"/>
            <shadow shadowOffsetGlobal="1" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="0.7" shadowColor="0,0,0,255" shadowRadiusAlphaOnly="0" shadowScale="100" shadowOffsetAngle="135" shadowDraw="0" shadowUnder="0" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowOffsetDist="1" shadowOffsetUnit="MM" shadowRadius="1.5" shadowRadiusUnit="MM"/>
            <substitutions/>
          </text-style>
          <text-format rightDirectionSymbol=">" autoWrapLength="0" decimals="3" wrapChar="" multilineAlign="4294967295" addDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" plussign="0" formatNumbers="0" reverseDirectionSymbol="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0"/>
          <placement centroidWhole="0" distMapUnitScale="3x:0,0,0,0,0,0" repeatDistance="0" repeatDistanceUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" priority="5" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" xOffset="0" placementFlags="10" offsetType="0" yOffset="0" centroidInside="0" maxCurvedCharAngleOut="-25" offsetUnits="MM" fitInPolygonOnly="0" quadOffset="4" distUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleIn="25" preserveRotation="1" placement="0" rotationAngle="0" dist="0"/>
          <rendering obstacleFactor="1" limitNumLabels="0" fontLimitPixelSize="0" scaleVisibility="0" maxNumLabels="2000" obstacle="1" labelPerPart="0" upsidedownLabels="0" scaleMin="0" fontMinPixelSize="3" fontMaxPixelSize="10000" obstacleType="0" drawLabels="1" displayAll="0" scaleMax="0" mergeLines="0" minFeatureSize="0" zIndex="0"/>
          <dd_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties" type="Map">
                <Option name="PositionX" type="Map">
                  <Option name="active" value="true" type="bool"/>
                  <Option name="expression" value="x(centroid(collect( $geometry, group_by:=is_selected())))" type="QString"/>
                  <Option name="type" value="3" type="int"/>
                </Option>
                <Option name="PositionY" type="Map">
                  <Option name="active" value="true" type="bool"/>
                  <Option name="expression" value="y(centroid(collect( $geometry, group_by:=is_selected())))" type="QString"/>
                  <Option name="type" value="3" type="int"/>
                </Option>
              </Option>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </dd_properties>
        </settings>
      </rule>
    </rules>
  </labeling>
  <layerGeometryType>2</layerGeometryType>
</qgis>
